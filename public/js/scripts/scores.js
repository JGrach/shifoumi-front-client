const scoreGraphValueDOM = document.getElementById('scoreGraphValue')
const scoreGraphXDOM = document.getElementById('scoreGraphX')
const scoreGraphYDOM = document.getElementById('scoreGraphY')


function loadScores(){
  getMeasures().then(function(res){
    displayScores(res.data)
  })
}

function displayScores(measures){
  scoreGraphXDOM.innerHTML = ''
  scoreGraphYDOM.innerHTML = ''
  delete measures.null // for now
  let totals = Object.keys(measures).map( key => {
    const total = measures[key].win + measures[key].equal + measures[key].loose
    const win = (measures[key].win / total * 100)
    const equal = (measures[key].equal / total * 100)
    const loose = (measures[key].loose / total * 100)
    scoreGraphYDOM.innerHTML += "<p>" + key + "</p>"
    scoreGraphValueDOM.innerHTML += 
    "<div class=scoreValueContainer>" +
      "<div id=" + key + " class=scoreValue>" + 
        "<div class=win scoreValueDetail style=height:" + win + "%>" /*+ (win === 0 ? '' : win + '%')*/ + "</div>" +
        "<div class=equal scoreValueDetail style=height:" + equal + "%>" /*+ (equal === 0 ? '' : equal + '%')*/ + "</div>" + 
        "<div class=loose scoreValueDetail style=height:" + loose + "%>" /*+ (loose === 0 ? '' : loose + '%')*/ + "</div>" +
      "</div>"
    "</div>"
    return { key, value: total }
  })
  const totalMax = totals.reduce( (sum, total) => sum + total.value, 0)
  totals.map( total => {
    const element = document.getElementById(total.key)
    element.style.height = (total.value / totalMax * 100) + "%"
  })
  const x = ['0%', '25%', '50%', '75%', '100%'].reverse()
  x.map( def => {
    scoreGraphXDOM.innerHTML += '<p>' + def + '</p>'
  })
}


loadScores()


