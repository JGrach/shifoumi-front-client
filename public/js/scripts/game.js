const gamePageDOM = document.getElementById('gamePage')

var GAME_PLAY = null
var GAME_LIST = null

const GAME_STATE = {
  list: function() {
    socketObserver.changeStatus.subscribe({
      name: 'gameStatePlay',
      func: this.play,
    })
    loadStyle('game/list/menu')
    loadStyle('game/list/table')
    loadStyle('game/list/creation')
    loadComponent('gameList', gamePageDOM).then(function(res) {
      killStyle('game/play/page')
      killStyle('game/play/info')
      killStyle('game/play/main')
      if (GAME_PLAY) {
        gamePlayKill()
      }
      if (!isExistScript('game/gameList')) {
        loadScript('game/gameList')
      } else {
        gameListCall()
      }
    })
  },
  play: function() {
    socketObserver.changeStatus.unsubscribe('gameStatePlay')
    loadStyle('game/play/page')
    loadStyle('game/play/info')
    loadStyle('game/play/main')
    loadComponent('gamePlay', gamePageDOM).then(function(res) {
      killStyle('game/list/menu')
      killStyle('game/list/table')
      killStyle('game/list/creation')
      if (GAME_LIST) {
        gameListKill()
      }
      if (!isExistScript('game/gamePlay')) {
        loadScript('game/gamePlay')
      } else {
        gamePlayCall()
      }
    })
  },
}

socketObserver.changeStatus.subscribe({
  name: 'gameStatePlay',
  func: GAME_STATE.play,
})
GAME_STATE.list()
