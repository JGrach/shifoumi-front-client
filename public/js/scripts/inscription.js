const formLoginDOM = document.getElementById('formLogin')
const nameDOM = document.getElementById('name')
const passDOM = document.getElementById('pass')
const emailDOM = document.getElementById('email')
const submitDOM = document.getElementById('submit')

submitDOM.addEventListener('click', function(e) {
  e.preventDefault()
  const user = {
    login: nameDOM.value,
    mailAddress: emailDOM.value,
    password: passDOM.value,
  }
  postUser(user).then(function(body) {
    sessionStorage.setItem('token', body.data.token)
    sessionStorage.setItem('username', body.data.user.login)
    sessionStorage.setItem('userId', body.data.user.id)
    window.location.href = 'http://' + URL.clientHost + '/game'
  })
})
