var socket = io(URL.gameHost + '/', {
  query: {
    token: sessionStorage.getItem('token'),
  },
})

const observable = {
  init: function() {
    this.observers = []
    this.last = {}
  },
  subscribe: function(func) {
    const isExist =
      this.observers.findIndex(function(obs) {
        return obs.name === func.name
      }) !== -1
    if (!isExist) {
      this.observers.push(func)
    }
  },
  unsubscribe: function(name) {
    this.observers = this.observers.filter(function(obs) {
      return obs.name !== name
    })
  },
  dispatch: function(element) {
    this.observers.map(function(obs) {
      obs.func(element)
    })
  },
}

const SO = {
  init: function() {
    this.listeners = [
      'list',
      'error',
      'changeStatus',
      'choose',
      'roundResolved',
      'roundClose',
    ]
    this.listeners.map(
      function(listener) {
        this[listener] = Object.create(observable)
        this[listener].init()
        socket.on(
          listener,
          function(newMessage) {
            const data =
              listener === 'error'
                ? newMessage.err
                  ? JSON.parse(newMessage.err)
                  : null
                : newMessage.data
                  ? JSON.parse(newMessage.data)
                  : null
            this[listener].history = data
            this[listener].dispatch(data)
          }.bind(this)
        )
      }.bind(this)
    )
  },
}

const socketObserver = Object.create(SO)
socketObserver.init()
socketObserver.error.subscribe({
  name: 'windowAlert',
  func: window.alert.bind(window),
})
