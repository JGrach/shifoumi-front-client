const gamePlayScript = {
  displayInitStatus: function(newStatus) {
    if (!newStatus) return
    this.statusGameDOM.innerHTML = newStatus.status
    this.rulesNameDOM.innerHTML = newStatus.rules.title
    this.rulesRoundNumberDOM.innerHTML = newStatus.rules.roundNumber
    this.rulesRoundTimeDOM.innerHTML =
      newStatus.rules.roundTime / 1000 + ' secondes'
    const user = newStatus.users.find(function(player) {
      return player.login == sessionStorage.getItem('username')
    })
    this.userRoundDOM.innerHTML = user.login
    this.playerLoad.then(res => {
      res.playersDOM[0].id = user.login
      res.identitiesDOM[0].innerHTML = user.login
      return res
    })
    if (newStatus.users.length > 1) {
      const resultsDOM = document.getElementsByClassName('result')
      for (var i = 0; i < resultsDOM.length; i++) {
        resultsDOM[i].innerHTML = ''
      }
      const adversary = newStatus.users.find(function(player) {
        return player.login != sessionStorage.getItem('username')
      })
      this.adversaryRoundDOM.innerHTML = adversary.login
      this.playerLoad.then(res => {
        res.playersDOM[1].id = adversary.login
        res.identitiesDOM[1].innerHTML = adversary.login
        return res
      })
      socketObserver.changeStatus.subscribe({
        name: 'displayStatus',
        func: this.displayStatus.bind(this),
      })
      socketObserver.changeStatus.unsubscribe('displayInitStatus')
    }
  },

  displayEndGame: function(status) {
    const winner = status.users.find(function(player) {
      return player.isWinner
    })
    this.gameDOM.innerHTML = ''
    this.gameDOM.style.display = 'block'
    this.gameDOM.innerHTML = winner
      ? '<p id=winnerDisplay>' + winner.login + ' a gagné ! </p>'
      : '<p id=winnerDisplay>Pas de gagnant !</p>'
    this.gameDOM.innerHTML += '<button id=back>Menu</button>'
    document.getElementById('back').addEventListener('click', function(e) {
      e.preventDefault()
      socket.emit('leaveGame')
      GAME_STATE.list()
    })
  },

  displayStatus: function(status) {
    console.log(status)
    this.statusGameDOM.innerHTML = status.status
    if (
      status.status == 'end' ||
      status.status == 'saved' ||
      status.status == 'deleted'
    )
      return this.displayEndGame(status)
    const i = status.rounds.length - 1
    const userGame = status.rounds[i].round.find(function(round) {
      return round.userId == sessionStorage.getItem('userId')
    })
    const adversaryGame = status.rounds[i].round.find(function(round) {
      return round.userId != sessionStorage.getItem('userId')
    })
    if (status.status == 'resolvingRound') {
      const weaponsDOM = document.getElementById('weapons')
      weaponsDOM.innerHTML = ''
    }
    if (status.status == 'resolvedRound') {
      this.roundGameDOM.innerHTML +=
        '<tr><td class=' +
        'winner_' +
        userGame.winner +
        '>' +
        (userGame.weapon ? userGame.weapon.display : 'Rien') +
        '</td><td class=' +
        'winner_' +
        adversaryGame.winner +
        '>' +
        (adversaryGame.weapon ? adversaryGame.weapon.display : 'Rien') +
        '</td></tr>'
      const resultDOM = document.getElementsByClassName('result')[1]
      resultDOM.innerHTML = adversaryGame.weapon
        ? adversaryGame.weapon.display
        : 'Rien'
    }
  },

  displayChoice: function(choice) {
    const weaponsHTML = choice.map(function(weapon) {
      return (
        '<button id=weapon_' +
        weapon.id +
        ' class="weaponSubmit">' +
        weapon.display +
        '</button>'
      )
    })
    const weaponsDOM = document.getElementById('weapons')
    const resultsDOM = document.getElementsByClassName('result')
    weaponsDOM.innerHTML = weaponsHTML.join(' \n')
    const weaponSubmit = document.getElementsByClassName('weaponSubmit')
    resultsDOM[0].innerHTML = 'nothing'
    resultsDOM[1].innerHTML = 'nothing'
    for (var i = 0; i < weaponSubmit.length; i++) {
      weaponSubmit[i].addEventListener('click', function(e) {
        e.preventDefault()
        const weaponId = e.target.id.substring(7)
        resultsDOM[0].innerHTML = choice[weaponId].display
        socket.emit('choice', weaponId)
      })
    }
  },

  call: function() {
    this.instantiate()
  },

  instantiate: function() {
    this.gameDOM = document.getElementById('game')
    this.statusGameDOM = document.getElementById('statusGameOutput')
    this.rulesNameDOM = document.getElementById('rulesName')
    this.rulesRoundNumberDOM = document.getElementById('rulesRoundNumber')
    this.rulesRoundTimeDOM = document.getElementById('rulesRoundTime')
    this.roundGameDOM = document.getElementById('roundsGameOutput')
    this.userRoundDOM = document.getElementById('userRoundTitle')
    this.adversaryRoundDOM = document.getElementById('adversaryRoundTitle')
    this.playerLoad = loadComponent('players', this.gameDOM).then(res => {
      return {
        playersDOM: document.getElementsByClassName('player'),
        identitiesDOM: document.getElementsByClassName('identity'),
      }
    })

    this.displayInitStatus(socketObserver.changeStatus.history)
    socketObserver.changeStatus.subscribe({
      name: 'displayInitStatus',
      func: this.displayInitStatus.bind(this),
    })
    socketObserver.choose.subscribe({
      name: 'displayChoice',
      func: this.displayChoice.bind(this),
    })
  },

  delete: function() {
    delete this.gameDOM
    delete this.statusGameDOM
    delete this.rulesNameDOM
    delete this.rulesRoundNumberDOM
    delete this.rulesRoundTimeDOM
    delete this.roundGameDOM
    delete this.userRoundDOM
    delete this.adversaryRoundDOM
    delete this.playerLoad

    socketObserver.changeStatus.unsubscribe('displayInitStatus')
    socketObserver.choose.unsubscribe('displayChoice')
    socketObserver.changeStatus.unsubscribe('displayStatus')
  },
}

function gamePlayCall() {
  GAME_PLAY = Object.create(gamePlayScript)
  GAME_PLAY.call()
}

function gamePlayKill() {
  GAME_PLAY.delete()
  GAME_PLAY = null
}

gamePlayCall()
