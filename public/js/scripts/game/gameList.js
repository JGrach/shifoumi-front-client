var GAME_CREATION = null

const gameListScript = {
  displayList: function(socketMessage) {
    if (!socketMessage) return
    this.gamesDOM.innerHTML = ''
    if (!socketMessage.length) {
      this.gamesDOM.innerHTML =
        '<td id="noFound" colspan=4>Aucune partie disponible actuellement</td>'
      return
    }
    socketMessage.map(
      function(game) {
        this.gamesDOM.innerHTML +=
          '<tr>\n' +
          '<td>' +
          game.users[0].login +
          '</td>\n' +
          '<td>' +
          game.rules.roundNumber +
          '</td>\n' +
          '<td>' +
          game.rules.roundTime / 1000 +
          ' secondes</td>\n' +
          '<td>' +
          '<input id=pass' +
          game.gameId +
          ' name=pass type=password placeholder=password></input>' +
          '<button id=' +
          game.gameId +
          '>Rejoindre</button></td>\n' +
          '</tr>\n'
        const passGameDOM = document.getElementById('pass' + game.gameId)
        const joinGameDOM = document.getElementById(game.gameId)
        joinGameDOM.addEventListener('click', function(e) {
          e.preventDefault
          const joinObject = { gameId: game.gameId }
          if (passGameDOM.value != '') {
            joinObject.pass = passGameDOM.value
          }
          socket.emit('joinGame', joinObject)
        })
      }.bind(this)
    )
  },

  call: function() {
    this.instantiate()
    this.createDOM.addEventListener(
      'click',
      function(e) {
        e.preventDefault()
        if (!document.getElementById('createSubmit')) {
          loadComponent('gameCreation', this.creationDOM).then(function(res) {
            if (!isExistScript('game/gameCreation')) {
              const async = true
              loadScript('game/gameCreation', async)
            } else {
              gameCreationCall()
            }
          })
          this.creationDOM.style.display = 'block'
        } else {
          this.creationDOM.style.display = 'none'
          this.creationDOM.innerHTML = ''
          gameCreationKill()
        }
      }.bind(this)
    )

    this.randomDOM.addEventListener('click', function(e) {
      e.preventDefault()
      socket.emit('joinGame')
    })
  },

  instantiate: function() {
    this.gamesDOM = document.getElementById('games')
    this.createDOM = document.getElementById('gameCreate')
    this.creationDOM = document.getElementById('gameCreation')
    this.randomDOM = document.getElementById('gameRandom')

    this.displayList(socketObserver.list.history)
    socketObserver.list.subscribe({
      name: 'displayList',
      func: this.displayList.bind(this),
    })
  },

  delete: function() {
    delete this.gamesDOM
    delete this.createDOM
    delete this.creationDOM
    delete this.randomDOM

    socketObserver.list.unsubscribe('displayList')
  },
}

function gameListCall() {
  GAME_LIST = Object.create(gameListScript)
  GAME_LIST.call()
}

function gameListKill() {
  if (GAME_CREATION) {
    gameCreationKill()
  }
  GAME_LIST.delete()
  GAME_LIST = null
}

gameListCall()
