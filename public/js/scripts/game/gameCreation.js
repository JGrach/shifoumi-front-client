const gameCreationScript = {
  call: function() {
    this.instantiate()

    getRules().then(
      function(rules) {
        rules = rules.data
        rules.map(
          function(rule) {
            this.rulesDOM.innerHTML +=
              '<option value=' +
              rule.id +
              '>' +
              rule.title +
              ' (' +
              rule.roundNumber +
              ' manches et ' +
              rule.roundTime / 1000 +
              ' secondes par coups)</option>'
          }.bind(this)
        )
      }.bind(this)
    )

    this.createSubmitDOM.addEventListener(
      'click',
      function(e) {
        e.preventDefault()
        const options = {}
        options.rulesId = this.rulesDOM.value
        if (!options.rulesId)
          return alert('Un jeu a toujours des règles, choisissez en une')
        if (this.createPassDOM.value != '') {
          options.pass = this.createPassDOM.value
        }
        socket.emit('createGame', options)
      }.bind(this)
    )
  },

  instantiate: function() {
    this.rulesDOM = document.getElementById('rules')
    this.createSubmitDOM = document.getElementById('createSubmit')
    this.createPassDOM = document.getElementById('createPass')
  },

  delete: function() {
    delete this.rulesDOM
    delete this.createSubmitDOM
    delete this.createPassDOM
  },
}

function gameCreationCall() {
  GAME_CREATION = Object.create(gameCreationScript)
  GAME_CREATION.call()
}

function gameCreationKill() {
  GAME_CREATION.delete()
  GAME_CREATION = null
}

gameCreationCall()
