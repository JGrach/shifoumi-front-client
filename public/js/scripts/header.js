const connectsDOM = document.getElementsByClassName('connect')
const disconnectsDOM = document.getElementsByClassName('disconnect')
const headerLinksDOM = document.getElementsByClassName('headerLink')

function clickLink(e){
  e.preventDefault()
  const link = e.target.id.split('_')
  if(link[0] === 'disconnection'){
    sessionStorage.setItem('token', null)
    sessionStorage.setItem('username', null)
    sessionStorage.setItem('userId', null)
    window.location = 'http://' + window.location.host + '/login'
    return
  }
  window.location = 'http://' + window.location.host + '/' + link[0]
}

if (sessionStorage.getItem('token') === null || sessionStorage.getItem('token') === "null"){
  for(connectDOM of connectsDOM){
    connectDOM.classList.add('hide')
  }
  for(disconnectDOM of disconnectsDOM){
    disconnectDOM.classList.add('visible')
  }
} else {
  for(connectDOM of connectsDOM){
    connectDOM.classList.add('visible')
  }
  for(disconnectDOM of disconnectsDOM){
    disconnectDOM.classList.add('hide')
  }
}

for(headerLinkDOM of headerLinksDOM){
  headerLinkDOM.addEventListener("click", clickLink)
}


