const formLoginDOM = document.getElementById('formLogin')
const nameDOM = document.getElementById('name')
const passDOM = document.getElementById('pass')
const submitDOM = document.getElementById('submit')

submitDOM.addEventListener('click', function(e) {
  e.preventDefault()
  login = {
    login: nameDOM.value,
    password: passDOM.value,
  }
  postLogin(login).then(function(body) {
    sessionStorage.setItem('token', body.data.token)
    sessionStorage.setItem('username', body.data.user.login)
    sessionStorage.setItem('userId', body.data.user.id)
    window.location.href = 'http://' + URL.clientHost + '/game'
  })
})
