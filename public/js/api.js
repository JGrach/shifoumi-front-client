function responseHdlr(req){
  return req.then(function(res) {
    return res.json()
  })
  .then(function(body) {
    if (!body.data || body.error)
      throw new Error('Body empty : ' + JSON.stringify(body.error))
    return body
  })
  .catch(function(err) {
    alert(JSON.stringify(err.message || err))
  })
}

function getHeaders() {
  const headers = new Headers
  headers.append('Content-Type', 'application/json')
  headers.append('mode', 'cors')
  return headers
}

function getRules() {
  const headers = getHeaders()
  headers.append('Authorization', 'bearer ' + sessionStorage.getItem('token'))
  const options = {
    method: 'GET',
    headers,
  }
  return responseHdlr(fetch('http://' + URL.apiHost + '/v0/rules', options))
}

function postUser(user) {
  const headers = getHeaders()
  const options = {
    method: 'POST',
    headers,
    body: JSON.stringify(user),
  }
  return responseHdlr(fetch('http://' + URL.apiHost + '/v0/user', options))
}

function postLogin(login) {
  const headers = getHeaders()
  const options = {
    method: 'POST',
    headers,
    body: JSON.stringify(login),
  }
  return responseHdlr(fetch('http://' + URL.apiHost + '/v0/login', options))
}

function getMeasures(type = 'quantity', pov, rules_id){
  const headers = getHeaders()
  headers.append('Authorization', 'bearer ' + sessionStorage.getItem('token'))
  const options = {
    method: 'GET',
    headers,
  }
  const query = []
  if(pov){
    query.push('pov=' + pov)
  }
  if(rules_id){
    query.push('rules_id=' + rules_id)
  }
  let queryString = query.join('&')
  if(queryString !== ''){
    queryString = '?'.concat(queryString)
  }
  return responseHdlr(fetch('http://' + URL.apiHost + '/v0/measure/' + type + queryString , options))
}