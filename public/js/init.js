// HTML Generation
function loadComponent(component, node) {
  return fetch('/components/' + component + '.html')
    .then(function(res) {
      return res.text()
    })
    .then(function(componentHTML) {
      node.innerHTML = componentHTML
    })
    .catch(function(err) {
      alert(err.message || err)
    })
}

// CSS Generation
function loadStyle(style){
  const styleDOM = document.createElement('link')
  styleDOM.rel = 'stylesheet'
  styleDOM.type = 'text/css'
  styleDOM.href = '/css/' + style + '.css'
  document.head.appendChild(styleDOM)
}

function killStyle(style){
  const stylesDOM = document.getElementsByTagName('link')
  for (var i = 0; i < stylesDOM.length; i++) {
    if (stylesDOM[i].href.search('/css/' + style + '.css') !== -1) {
      stylesDOM[i].parentNode.removeChild(stylesDOM[i])
      return true
    }
  }
  return false
}

// JS Generation
function loadScript(script, async = true) {
  const scriptDOM = document.createElement('script')
  scriptDOM.async = async
  scriptDOM.src = '/js/scripts/' + script + '.js'
  scriptDOM.type = 'text/javascript'
  document.head.appendChild(scriptDOM)
}

function isExistScript(script) {
  const scriptsDOM = document.getElementsByTagName('script')
  for (var i = 0; i < scriptsDOM.length; i++) {
    if (scriptsDOM[i].src.search('/js/scripts/' + script + '.js') !== -1) {
      return true
    }
  }
  return false
}

// Socket call
function loadSocket() {
  const socketIo = document.createElement('script')
  socketIo.async = false
  socketIo.src = 'http://' + URL.gameHost + '/socket.io/socket.io.js'
  socketIo.type = 'text/javascript'
  document.head.appendChild(socketIo)
}

fetch('/components/header.html')
  .then(function(res) {
    return res.text()
  })
  .then(function(html) {
    loadStyle("header")
    document.body.innerHTML += html
  })
  .then(function() {
    fetch('/pages/' + PAGE_NAME + '.html')
      .then(function(res) {
        return res.text()
      })
      .then(function(html) {
        document.body.innerHTML += html
        if (PAGE_NAME == 'game') {
          loadSocket()
          loadScript('game/socketHandler', false)
        }
        loadScript(PAGE_NAME, PAGE_NAME != 'game')
        loadScript("header")
      })
      .catch(function(err) {
        alert(err.message || err)
      })
  })
  .catch(function(err) {
    alert(err.message || err)
  })
  
