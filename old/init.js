const messagesDOM = document.getElementById('messageDisplay')
const choosesDOM = document.getElementById('chooseDisplay')
const gamesDOM = document.getElementById('gameDisplay')
const menuDOM = document.getElementById('menu')
let cancelDOM = document.getElementById('actionCancel')

const socketStorage = []

const socket = io('localhost:8888/', {
  query: {
    token: sessionStorage.getItem('token'),
  },
})

const listeners = [
  'message',
  'error',
  'newStatus',
  'choose',
  'roundResolved',
  'roundClose',
]

const observer = {
  init: function() {
    this.observers = []
  },
  subscribe: function(func) {
    const isExist =
      this.observers.findIndex(function(obs) {
        return obs.name === func.name
      }) !== -1
    if (!isExist) {
      this.observers.push(func)
    }
  },
  unsubscribe: function(name) {
    this.observers = this.observers.filter(function(obs) {
      return obs.name !== name
    })
  },
  dispatch: function(element) {
    this.observers.map(function(obs) {
      obs.func(element)
    })
  },
}

function displayChoose(element) {
  const message = element.message ? element.message : '<p>no data</p>'
}

function displayGame(element) {
  function clickCancelDOM(e) {
    console.log('test')
    socket.emit('delete')
    gamesDOM = ''
    menuDOM.style.display = 'flex'
  }
  let message = ''
  if (!element.message) {
    message = '<p>no data</p>'
  } else {
    message =
      '<p>Jeu numéro: ' +
      element.message.uuid +
      '</p>' +
      '<p>Status: ' +
      element.message.status +
      '</p>' +
      '<p>Players: ' +
      element.message.players.map(function(player) {
        return player.login + ' '
      }) +
      '</p>' +
      element.message.round.map(function(r, i) {
        return '<p>Round ' + i + ': ' + r + '</p>'
      }) +
      '<p>Rules: ' +
      element.message.rules.roundNumber +
      ' rounds de ' +
      element.message.rules.roundTime / 1000 +
      ' secondes chacun</p>' +
      '<p>Password: ' +
      element.message.pass +
      '</p>'
  }
  if (element.message.status == 'wait_for_player') {
    cancelDOM.style.display = 'block'
    cancelDOM = document.getElementById('actionCancel')
    cancelDOM.click(clickCancelDOM)
    console.log(cancelDOM)
  } else {
    cancelDOM.style.display = 'none'
    cancelDOM = document.getElementById('actionCancel')
    cancelDOM.click(clickCancelDOM)
    console.log(cancelDOM)
  }
  if (element.message.status == 'gameResolved') {
    menuDOM.style.display = 'block'
  }
  gamesDOM.innerHTML = message
}

function displayMessage(element) {
  const message = element.message ? JSON.stringify(element.message) : 'no data'
  const origin = element.origin
    ? "<li><span id='origin'>" + element.origin + '</span> => '
    : ''
  const event = element.event
    ? "<span id='" +
      element.event +
      "' class='event'>" +
      element.event +
      '</span>: '
    : ''
  messagesDOM.innerHTML += origin + event + message + '</li>\n'
}

function alertError(element) {
  alert(element.message)
}

const listened = listeners.reduce(function(acc, listener) {
  acc[listener] = Object.create(observer)
  acc[listener].init()
  socket.on(listener, function(newMessage) {
    acc[listener].dispatch({
      origin: 'server',
      message:
        listener === 'error'
          ? newMessage.err
            ? JSON.parse(newMessage.err)
            : null
          : newMessage.data
            ? JSON.parse(newMessage.data)
            : null,
      listener: listener,
    })
  })
  return acc
}, {})

listened.error.subscribe({ name: 'alertError', func: alertError })
listened.message.subscribe({ name: 'displayMessage', func: displayMessage })
listened.choose.subscribe({ name: 'displayChoose', func: displayChoose })

listened.newStatus.subscribe({ name: 'displayGame', func: displayGame })
listened.roundResolved.subscribe({ name: 'displayGame', func: displayGame })
