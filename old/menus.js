const optionsDOM = document.getElementById('options')
const menuOptions = ['actionList', 'actionCreate', 'actionJoin']

const create = {
  form:
    "<select id='rules'><option value= '' disabled selected>Choisir la Règle</option></select>" +
    "<input id='createPass' type='password' placeholder='password (facultatif)' ></input>" +
    "<input id='createSubmit' type='submit' value='Créer' ></input>",
  init() {
    const haveRules = this.haveRules()
    haveRules
      .then(
        function(rules) {
          rules = rules.data
          optionsDOM.innerHTML = this.form
          this.rulesDOM = document.getElementById('rules')
          this.createPassDOM = document.getElementById('createPass')
          this.createSubmitDOM = document.getElementById('createSubmit')
          this.createSubmitDOM.addEventListener(
            'click',
            this.clickcreateSubmitDOM.bind(this)
          )
          const rulesValues = rules.map(
            function(rule) {
              return this.displayRule(rule)
            }.bind(this)
          )
          this.rulesDOM.innerHTML += rulesValues.concat()
          optionsDOM.style.display = 'flex'
        }.bind(this)
      )
      .catch(function(err) {
        alert(JSON.stringify(err))
      })
  },
  destroy() {
    optionsDOM.style.display = 'none'
    if (this.createSubmitDOM) {
      this.createSubmitDOM.removeEventListener(
        'click',
        this.clickcreateSubmitDOM
      )
    }
  },
  haveRules: function() {
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')
    headers.append('Authorization', 'bearer ' + sessionStorage.getItem('token'))
    const options = {
      method: 'GET',
      headers,
    }
    return fetch('http://localhost:8080/v0/rules', options).then(function(res) {
      return res
        .json()
        .then(function(res) {
          if (res.data && res.data.error) throw res.data.error
          return res
        })
        .catch(function(err) {
          return err
        })
    })
  },
  displayRule: function(rule) {
    return (
      '<option value=' +
      rule.id +
      '>' +
      rule.title +
      ' (' +
      rule.roundNumber +
      ' manches et ' +
      rule.roundTime / 1000 +
      ' secondes par coups)</option>'
    )
  },
  clickcreateSubmitDOM: function(e) {
    console.log('bonjour')
    e.preventDefault()
    const options = {}
    options.rulesId = this.rulesDOM.value
    if (!options.rulesId)
      return alert('Un jeu a toujours des règles, choisissez en une')
    if (this.createPassDOM.value != '') options.pass = this.createPassDOM.value
    socket.emit('create', options)
  },
}

const join = {
  form:
    "<select id='list'><option value= ''>Jeu au Hasard</option></select>" +
    "<input id='joinPass' type='password' placeholder='password (si la partie a été crée avec un password)' ></input>" +
    "<input id='joinSubmit' type='submit' value='Rejoindre'></input>",
  init: function() {
    optionsDOM.innerHTML = this.form
    this.listDOM = document.getElementById('list')
    this.joinPassDOM = document.getElementById('joinPass')
    this.joinSubmitDOM = document.getElementById('joinSubmit')
    this.joinSubmitDOM.addEventListener(
      'click',
      this.clickJoinSubmitDOM.bind(this)
    )
    listened.message.subscribe({
      name: 'listGame',
      func: this.listGames.bind(this),
    })
    socket.emit('list')
  },
  clickJoinSubmitDOM: function(e) {
    e.preventDefault()
    const game = this.listDOM.value ? { gameId: this.listDOM.value } : null
    if (this.joinPassDOM.value != '') game.pass = this.joinPassDOM.value
    socket.emit('join', game)
  },
  destroy: function() {
    optionsDOM.style.display = 'none'
    listened.message.unsubscribe('listGames')
    if (this.joinSubmitDOM) {
      this.joinSubmitDOM.removeEventListener('click', this.clickJoinSubmitDOM)
    }
  },
  listGames: function(element) {
    const listDOM = document.getElementById('list')
    if (Array.isArray(element.message) && element.message.length !== 0) {
      const listValues = element.message.map(
        function(game) {
          return this.displayGame(game)
        }.bind(this)
      )
      listDOM.innerHTML += listValues.concat()
    }
    optionsDOM.style.display = 'flex'
  },
  displayGame: function(game) {
    return '<option value=' + game.uuid + '>' + game.owner + '</option>'
  },
}

menuOptions.map(function(menu) {
  document.getElementById(menu).addEventListener('click', function(e) {
    e.preventDefault()
    const joinOloo = Object.create(join)
    const createOloo = Object.create(create)
    switch (menu) {
      case 'actionCreate':
        joinOloo.destroy()
        createOloo.init()
        menuDOM.style.display = 'none'
        break
      case 'actionJoin':
        createOloo.destroy()
        joinOloo.init()
        menuDOM.style.display = 'none'
        break
      default:
        socket.emit('list')
    }
  })
})
