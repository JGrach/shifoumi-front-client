const weaponDOM = document.getElementById('weapon')

const runner = {
  init(element) {
    if (this.status != 'run' && element.message.status == 'run') {
      this.status = 'run'
      menuDOM.style.display = 'none'
      weaponDOM.style.display = 'flex'
      listened.choose.subscribe({
        name: 'choose',
        func: this.choose.bind(this),
      })
    } else if (this.status == 'run' && element.message.status == 'delete') {
      this.destroy()
    }
  },
  choose(element) {
    const weapons = element.message
    weapons.map(function(weapon) {
      const weaponDOM = document.getElementById(weapon.display)
      weaponDOM.addEventListener('click', function(e) {
        e.preventDefault()
        socket.emit('choice', weapon.id)
      })
    })
  },
  destroy() {
    this.status = 'stop'
    listened.choose.unsubscribe('choose')
    listened.newStatus.unsubscribe('runnerInit')
    menuDOM.style.display = 'flex'
    weaponDOM.style.display = 'none'
  },
}

listened.newStatus.subscribe({
  name: 'runnerInit',
  func: runner.init.bind(runner),
})
