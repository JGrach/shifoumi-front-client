const express = require('express'),
  router = express.Router(),
  { waterfall } = require('async'),
  fs = require('fs')

router.use(express.static('public'))

router.get('/:pageName', (req, res) => {
  const { pageName } = req.params
  waterfall(
    [
      cb => {
        fs.readdir('./public/pages', (err, files) => {
          const isExist = files.find(file => file == pageName + '.html')
          if (isExist) return cb(null)
          cb('erreur 404') // TODO format
        })
      },
      cb => {
        fs.readFile('./view/main.html', 'utf8', cb)
      },
      (html, cb) => {
        html = html
          .replace(/{{PAGE_NAME}}/g, pageName)
          .replace(/{{API_HOST}}/g, process.env.API_IP)
          .replace(/{{CLIENT_HOST}}/g, process.env.FRONT_IP)
          .replace(/{{GAME_HOST}}/g, process.env.GAME_ENGINE_IP)
        cb(null, html)
      },
    ],
    (err, html) => {
      if (err) return res.send(err)
      res.send(html)
    }
  )
})

module.exports = router
