const router = require('./router')
const app = require('express')(),
  http = require('http').Server(app),
  env = process.env.NODE_ENV || "development",
  { port } = require('./config')[env]

if(env === 'development') require('dotenv').config()

app.use(router)
http.listen(port)
