FROM node:latest

# Create app directory
WORKDIR /usr/src/shifoumi

ADD . .

RUN npm update && npm install

EXPOSE 3000
ENTRYPOINT ["npm", "run"]
CMD [ "start" ]
